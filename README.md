# ИНСТРУКЦИЯ

# Зависимости
- node
- docker
- docker-compose

# Установка
- npm i
- npm run dev:env - дождаться запуска контейнеров
- npm run dev:start - запуск всех процессов
- http://localhost:9000/

# Трудозатраты 16ч

- 1h адаптивная верстка при помощи vuetify
- 1h окружение, подготовка проекта
- 2h серверна часть, начало, подключения
- 1h разбивка верстки на компоненты слои страницы
- 2h валидация форм на клиенте
- 1h loader и snackbar и api
- 8h серверная часть и worker
