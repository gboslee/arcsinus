const amqplib = require('amqplib')
const util = require('util')
const pg = require('pg')
const logger = require('../backend/lib/logger.js')

const broker = 'rpc'
const schema = {
  user: `CREATE TABLE IF NOT EXISTS users (
  ID SERIAL PRIMARY KEY,
  login VARCHAR(15),
  password VARCHAR(500),
  fullname VARCHAR(40),
  email VARCHAR(30),
  phone VARCHAR(12),
  city VARCHAR(50),
  country VARCHAR(50),
  osphone VARCHAR(20)
);`,
  index: `CREATE UNIQUE INDEX IF NOT EXISTS ix_login ON users (login);`
}

const pool = new pg.Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'mysecretpassword',
  port: 5432
})

pool.query = util.promisify(pool.query)

const core = {
  logger,
  pool
}

const method = {
  'auth.signin': require('./api/auth.signin.js'),
  'auth.signup': require('./api/auth.signup.js')
}

;(async () => {
  try {
    await pool.query(schema.user)
    await pool.query(schema.index)
  } catch (error) {
    pool.end()
    logger.fatal(error)
    return
  }
  amqplib
    .connect()
    .then(connection => {
      //process.once('SIGINT', () => connection.close())
      connection
        .createChannel()
        .then(channel => {
          return channel
            .assertQueue(broker, {
              durable: false
            })
            .then(() => {
              channel.prefetch(1)
              return channel.consume(broker, message => {
                const pack = JSON.parse(message.content.toString())
                method[pack.method](core, pack.param, data => {
                  channel.sendToQueue(
                    message.properties.replyTo,
                    Buffer.from(JSON.stringify(data)),
                    { correlationId: message.properties.correlationId }
                  )
                  channel.ack(message)
                })
              })
            })
        })
        .catch(logger.error)
    })
    .catch(error => {
      pool.end()
      logger.fatal(error)
    })
})()
